import socket
import threading
import time
from Queue import Queue

global end
end = False

MESSAGE_LENGHT = 25
HOST           = '127.0.0.1'
PORT           = 1235
BUFFER_SIZE    = 2048
NODES          = 2

def padded(message, message_lenght = MESSAGE_LENGHT):
	''' Padding the message with spaces to obtain the necessary message_lenght '''
	return '{mess: <{fill}}'.format(mess = message[:message_lenght], fill = message_lenght)

CONSUMER = padded('0') # Receiver
PRODUCER = padded('1') # Sender


class Socket_wrapper:

	host = HOST
	port = PORT
	buffer_size = BUFFER_SIZE

	def __init__(self, new_socket = None):
		print 'Creating..',

		if new_socket is None:
			self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		else:
			self.socket = new_socket

		print str(self.socket)

	def connect(self, host = None, port = None):
		if host is None:
			host = self.host
		if port is None:
			port = self.port

		self.socket.connect((host, port))
		print 'Conected to: {}:{}'.format(host, port)

	def send(self, message, message_lenght = MESSAGE_LENGHT):

		message = padded(message)
		total_sent = 0

		while total_sent < message_lenght:
			sent = self.socket.send(message[total_sent:])

			if sent == 0:
				raise RuntimeError("socket connection broken")

			total_sent = total_sent + sent

		print 'Sent:', message

	def receive(self, message_lenght = MESSAGE_LENGHT):
		chunks = []
		bytes_recorded = 0

		while bytes_recorded < message_lenght:
			chunk = self.socket.recv(min(message_lenght - bytes_recorded, self.buffer_size))
			
			if chunk == '':
				raise RuntimeError("socket connection broken")
			
			chunks.append(chunk)
			bytes_recorded = bytes_recorded + len(chunk)

		return ''.join(chunks)


		# if buffer_size is None:
		# 	buffer_size = self.buffer_size

		# print 'in receive0'

		# data = ''
		# while True:
		# 	print 'in receive1'
		# 	data_bit = self.socket.recv(buffer_size)
		# 	print 'in receive2'

		# 	if not data_bit:
		# 		break

		# 	print 'in receive3'
		# 	data += data_bit

		# print 'Received:', data
		# return data
		  
	def close(self):
		print 'Closing connection ', str(self)
		self.socket.close()
	

	def bind(self):
		print 'Binding..'
		self.socket.bind( (self.host, self.port) )

	def listen(self, queue_lenght):
		print 'Listening..'
		self.socket.listen(queue_lenght)

	def accept(self):
		print 'Accepting.. ',
		connection, address = self.socket.accept()
		return Socket_wrapper(connection), address


class Socket_listener(threading.Thread):

	def __init__(self, socket, broker_queue):
		threading.Thread.__init__(self)
		self.socket = socket
		self.broker_queue = broker_queue

	def run(self):

		self.socket.bind()
		self.socket.listen(3)
		
		while not end:

			connection, addr = self.socket.accept()
			who_are_you = connection.receive()
			node = int(connection.receive())
			print who_are_you, node

			if node >= NODES:
				print "What are you?"
				continue

			if who_are_you == CONSUMER:
				connection.send('Greetings Receiver')
				print 'Hello Consumer'

				self.broker_queue[node].append(Queue())

				consumer_thread = Consumer_thread(connection, self.broker_queue[node][-1])
				consumer_thread.start()

			elif who_are_you == PRODUCER:
				connection.send('Greetings Sender')
				print 'Hello Producer'

				producer_thread = Producer_thread(connection, self.broker_queue[node])
				producer_thread.start()

			else:
				print "What are you?"
		

class Producer_thread(threading.Thread):

	def __init__(self, socket, broker_queue):
		threading.Thread.__init__(self)
		self.socket = socket
		self.broker_queue = broker_queue

	def run(self):

		print 'Imma producer'

		while not end:
			data = self.socket.receive()
			for consumer_queue in self.broker_queue:
				consumer_queue.put(data)

			print '%s put to brokers_queue by %s' % (data, self.name)

		self.socket.close()


class Consumer_thread(threading.Thread):

	def __init__(self, socket, broker_queue):
		threading.Thread.__init__(self)
		self.socket = socket
		self.broker_queue = broker_queue

	def run(self):
		print 'Imma consumer'

		while not end:
			data = self.broker_queue.get()
			self.broker_queue.task_done()
			self.socket.send(data)
			print '%s popped from broker_queue by %s' % (data, self.name)

		self.socket.close()
