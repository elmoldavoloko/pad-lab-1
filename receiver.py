# Receiver
from library import *

if __name__ == '__main__':
	receiver_socket = Socket_wrapper()
	receiver_socket.connect()

	receiver_socket.send(CONSUMER)
	receiver_socket.send('1')
	print receiver_socket.receive()
	
	while True:
		data = receiver_socket.receive()
		print 'Received: ', data
		if data == padded('bye'):
			break
	
	receiver_socket.close()
